import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Call } from './call'

@Injectable({
  providedIn: 'root'
})
export class CallService {
  private apiUrl: string = 'http://localhost:3000'
  private callId: number = null
  constructor(private http: HttpClient) { }
  placeCall(number: string) {
    const postData = { number1: '999999999', number2: number }
    this.http.post<Call>(this.apiUrl + '/call', postData).subscribe(data => {
      this.callId = data.id
    });
  }
}
