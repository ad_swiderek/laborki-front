import { Component, OnInit } from '@angular/core';
import { CallService } from '../call.service'

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.css']
})

export class WidgetComponent implements OnInit {
  number: string
  validator = /(^[0-9]{9}$)/

  constructor(private callService: CallService) { }

  ngOnInit() {
  }

  call() {
    if (this.isValidNumber()) {
      this.callService.placeCall(this.number)
    } else {
      console.info("Numer niepoprawny")
    }
  }

  isValidNumber(): Boolean {
    /*if (this.number.length == 9)
      return true
    else
      return false*/

      return this.validator.test(this.number)
  }
}
